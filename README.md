# User Session Challenge App

User Session Challenge App is a project created to manage user session token generation. Is a client side project for [User Session Challenge Api](https://gitlab.com/Ara_Manjon/user-session-challenge).

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### Installation

> git clone git@gitlab.com:Ara_Manjon/user-session-challenge-app.git
>
> In the project directory, you can run:
>
> yarn start
>
> visit [http://localhost:3000](http://localhost:3000)
--------------------------

### Description

From an inside-out TDD approach, the first step was create logic with http request and project has been growing doing unit test in all work flow.

> In the project directory, you can run:
>
> yarn test

##### Requirements:

- One screen where add a username and a submit button

- Return username with current date and token


##### Constraints:

:x: Username should has a valid format.
